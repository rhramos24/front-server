module.exports = (grunt) ->
  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")

    clean:
      build: ["tmp/build/js", "tmp/amd"]

    uglify:
      options:
        banner: ""
        mangle: false
      build: 
        files: 
          'public/js/vendor.js': [
            'dist/vendor.js'
          ]
          'public/js/app.js': [
            'dist/app.js'
          ]

    jade: 
      compile: 
        options: 
          pretty: true
          data: 
            debug: true
        files: [
          expand: true
          cwd: "app/jade_templates"
          src: "**/**/*.jade"
          ext: ".html"
          dest: "app/templates"
        ]

    coffee: 
      options:
        bare: true
      compile: 
        files: [
          expand: true
          src: "**/*.coffee"
          cwd: "app"
          ext: ".js"
          dest: "tmp/build/js"
        ]

    sass:
      dist: 
        files: [
          expand: true
          src: 'app/styles/*.sass'
          dest: 'tmp/build/css'
          ext: '.css'
        ]

    cssmin: 
      minify: 
        files: 
          'public/css/vendor.css': [
            'dist/vendor.css'
          ]
          'public/css/app.css': [
            'dist/app.css'
          ]

    
    copy:
      main:
        files: [
          {
            expand: true
            cwd: "app/templates"
            src: ["**/**/*.html"]
            dest: "app/assets"
            filter: "isFile"
          }
          {
            expand: true
            cwd: "app/assets"
            src: ["**/*"]
            dest: "public"
            filter: "isFile"
          }
          
        ]
    
        

    bower: 
      install: 
        options:
          targetDir: 'tmp/build/vendor'
          layout: 'byType'
          install: true
          verbose: false
          cleanTargetDir: false
          cleanBowerDir: false
          bowerOptions: {}



    concat:
      dev: 
        files: 
          'dist/vendor.js': ["tmp/build/vendor/**/*.js"]
          'dist/app.js': ["tmp/build/js/**.js"]
          'dist/vendor.css': ['tmp/build/vendor/**/*.css']
          'dist/app.css': ['tmp/build/css/**/*.css']

      build:
        files:
           'public/js/vendor.js': ["dist/vendor.js"]
           'public/js/app.js': ['dist/app.js']
           'public/css/vendor.css': ["dist/vendor.css"]
           'public/css/app.css': ['dist/app.css']
    
    watch: 
      scripts: 
        files: 'app/**/*.coffee'
        tasks: ['coffee', "concat", "copy"]
        options: 
          livereload: true
      css: 
        files: 'app/styles/*.sass'
        tasks: ['sass',"cssmin"]
        options: 
          livereload: true
   

    connect: 
      server: 
        options: 
          port: 3333
          hostname: '*'
          livereload: true
          keepalive: false
          base: 'public'



  grunt.loadNpmTasks('grunt-bower-task')
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks("grunt-contrib-uglify")
  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-contrib-jade')
  grunt.loadNpmTasks('grunt-contrib-cssmin')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-connect')
  grunt.loadNpmTasks('grunt-contrib-clean')
  grunt.loadNpmTasks('grunt-contrib-sass');


  
  grunt.registerTask 'deploy', 'Deploy Task', (enviroment, target) ->
    basicTasks = ["clean","coffee", "jade", "sass", "concat:dev"]
    grunt.log.writeln("== Deploy Task Running ==")
    switch enviroment  
      when "prod"
        grunt.log.writeln("== Setting production tasks ==")
        basicTasks.push("cssmin")
        basicTasks.push("concat:dev")
        basicTasks.push("uglify")
        basicTasks.push("copy")
      else 
        grunt.log.writeln("== Setting development tasks ==")
        basicTasks.push("concat")
        basicTasks.push("copy")

    switch target  
      when "cordova"
        # TODO
      else 
        grunt.log.writeln("== Target server ==")
        basicTasks.push("connect")
        basicTasks.push("watch")
        
        

    grunt.task.run(basicTasks)
    
  # Default task(s).
  grunt.registerTask "default", ["deploy"]